import unittest
from gitsvnext.svn import SVNINFO_URL, SVNINFO_REPO_ROOT, parse_external, resolve_external

SVNINFO = {
    SVNINFO_URL: 'http://example.com/svn/repos-1/project1/trunk',
    SVNINFO_REPO_ROOT: 'http://example.com/svn/repos-1'
}

def _parse_and_resolve(e, parent=''):
    e = parse_external(e)
    e = resolve_external(e, SVNINFO)
    return e

class ExternalsResolutionTest(unittest.TestCase):    
    def test_old_format(self):
        e = _parse_and_resolve('foo http://example.com/repos/zig')
        self.assertEqual(e.source, 'http://example.com/repos/zig')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'foo')
        
        e = _parse_and_resolve('foo/bar -r 1234 http://example.com/repos/zag')
        self.assertEqual(e.source, 'http://example.com/repos/zag')
        self.assertEqual(e.source_rev, '1234')
        self.assertEqual(e.target, 'foo/bar')
        
    def test_old_format_peg(self):
        e = _parse_and_resolve('foo http://example.com/repos/zig@HEAD')
        self.assertEqual(e.source, 'http://example.com/repos/zig@HEAD')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'foo')
        
        e = _parse_and_resolve('foo/bar -r 1234 http://example.com/repos/zag@HEAD')
        self.assertEqual(e.source, 'http://example.com/repos/zag@HEAD')
        self.assertEqual(e.source_rev, '1234')
        self.assertEqual(e.target, 'foo/bar')
        
    def test_new_format(self):
        e = _parse_and_resolve('http://example.com/repos/zig foo1')
        self.assertEqual(e.source, 'http://example.com/repos/zig')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'foo1')
        
        e = _parse_and_resolve('-r 1234 http://example.com/repos/zag foo/bar1')
        self.assertEqual(e.source, 'http://example.com/repos/zag')
        self.assertEqual(e.source_rev, '1234')
        self.assertEqual(e.target, 'foo/bar1')
        
    def test_dir_relative(self):
        e = _parse_and_resolve('../../project2/trunk common/project2/trunk')
        self.assertEqual(e.source, 'http://example.com/svn/repos-1/project2/trunk')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'common/project2/trunk')
        
    def test_repository_relative(self):
        e = _parse_and_resolve('^/project2/trunk common/project2/trunk')
        self.assertEqual(e.source, 'http://example.com/svn/repos-1/project2/trunk')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'common/project2/trunk')
        
        e = _parse_and_resolve('^/../repos-2/foo/trunk common/foo/trunk')
        self.assertEqual(e.source, 'http://example.com/svn/repos-2/foo/trunk')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'common/foo/trunk')
        
    def test_scheme_relative(self):
        e = _parse_and_resolve('//example.com/svn/repos-1/project2/trunk common/project2/trunk')
        self.assertEqual(e.source, 'http://example.com/svn/repos-1/project2/trunk')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'common/project2/trunk')
        
    def test_server_relative(self):
        e = _parse_and_resolve('/svn/repos-1/project2/trunk common/project2/trunk')
        self.assertEqual(e.source, 'http://example.com/svn/repos-1/project2/trunk')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'common/project2/trunk')
        
    def test_unspecified_relative(self):
        # not sure whether this is valid at all or not
        e = _parse_and_resolve('subfolder/foo foo')
        self.assertEqual(e.source, 'http://example.com/svn/repos-1/project1/trunk/subfolder/foo')
        self.assertEqual(e.source_rev, None)
        self.assertEqual(e.target, 'foo')
    
    