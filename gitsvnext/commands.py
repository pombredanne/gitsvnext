#!/usr/bin/env python
from __future__ import print_function

import mcommand
import os
import re
import shutil
import sys

from . import git, svn
from .util import join_url

CFG_DIR = 'svnext'
   
def _externals_filepath(repo):
    return os.path.join(repo.git_dir, CFG_DIR, 'externals')
    
def _makedirs(path):
    if not os.path.isdir(path):
        os.makedirs(path)
    
def _create_file(path, *args, **kwargs):
    '''behaves as open() except is creates the path to file
    '''
    _makedirs(os.path.dirname(path))
    
    return open(path, *args, **kwargs)
    
def _replace_file(path, replacement_path, bak_path=None):
    if not bak_path:
        bak_path = path + '.bak'
        
    if os.path.exists(bak_path):
        os.remove(bak_path)
    
    if os.path.exists(path):
        _makedirs(os.path.dirname(path))
        os.rename(path, bak_path)        
    
    _makedirs(os.path.dirname(path))
    os.rename(replacement_path, path)
    
       
def retrieve_externals(repo, extpath=None):
    '''retrieves externals from SVN and stores it in the file named extpath
    '''
    repo.ui.status('Loading externals definitions from SVN...', "\n")
    
    externals = repo.svn('show-externals')
    
    if not extpath:
        extpath = _externals_filepath(repo)
    
    extpath_new = extpath + '.new'
       
    with _create_file(extpath_new, 'w') as extfile:
        extfile.write(externals)
        
    _replace_file(extpath, extpath_new)
        
def refresh_externals(repo, exts):
    '''refresh definition of given externals
    '''
    svn_info = repo.svn_info()
    root = svn_info[svn.SVNINFO_URL].rstrip('/\\')
    targets = set(join_url(root, e.parent) for e in exts)
    svn_rev = repo.svn_rev or svn_info[svn.SVNINFO_REVISION]
    
    svnclient = svn.SvnClient(repo)
    
    for parent, exts in svnclient.retrieve_externals(targets, rev=svn_rev):
        assert parent.startswith(root)
        parent = parent[len(root)+1:]
        yield parent, exts
                
def load_externals(repo, reload=False, refresh_known=True):
    '''Loads externals definition from a file with git svn show-externals format
    '''
    svninfo = repo.svn_info()
    
    extpath = _externals_filepath(repo)
    reload = reload or not os.path.exists(extpath)
    
    if reload:
        retrieve_externals(repo, extpath)
        refresh_known = False # no need for that 
        
    def parse_externals(exts):
        for parent, externals in exts:
            parent = parent.replace('\\', '/').lstrip('/')
            for e in externals:
                if not e: continue
                e = svn.parse_external(e)
                e.parent = parent.replace('\\', '/')
                yield e
    
    exts = parse_externals(git.read_svn_externals(open(extpath)))
    
    if refresh_known:
        repo.ui.note('Refreshing definitions of known externals...', '\n')
        exts = parse_externals(refresh_externals(repo, exts))
    
    exts = (svn.resolve_external(e, svninfo) for e in exts)
    
    # simulate svn behaviour when later definition overwrites former one
    exts_by_target = dict((e.full_target, e) for e in exts)
           
    exts = sorted(exts_by_target.itervalues(), key=lambda e: e.full_target.lower())
    
    if reload:
        repo.ui.status('Updating ignores...', '\n')
        # externals were reloaded --> update ignores
        ignores = (e.full_target for e in exts)
        write_ignores(repo, ignores)
        
    return exts
            
def update_externals(repo, exts, force_init=False):
    svnclient = svn.SvnClient(repo)
    
    def clean_dir(path):
        shutil.rmtree(path)
    
    def init_svn_dir(path, ext):
        repo.ui.status("Initing '{0}':".format(ext.full_target), '\n')
        args = ['checkout']
        if ext.source_rev:
            args += [ext.source + '@' + ext.source_rev]
        else:
            args += [ext.source]
        args += [ext.full_target]
        svnclient.call(args, cwd=repo.repo_root)
        repo.ui.status('\n')
    
    def update_svn_dir(path, ext):
        url = svnclient.read_info(path)[svn.SVNINFO_URL]
        if url == ext.source: # plain update
            args = ['update', ext.full_target]  
        else: # a repo switch is needed
            repo.ui.note('A repo switch is needed -  "{0}" differs from current url "{1}"'.format(ext.source, url), '\n')
            args = ['switch', ext.source, ext.full_target]
            args += ['--ignore-ancestry']
            repo.ui.status("Switching '{0}':".format(ext.full_target), '\n')
        
        if ext.source_rev:
            args += ['-r', ext.source_rev] 
                
        svnclient.call(args, cwd=repo.repo_root)
        repo.ui.status('\n')
        
    for e in exts:
        f = os.path.join(repo.repo_root, e.full_target)
        if os.path.isdir(f):
            if os.path.isdir(os.path.join(f, '.svn')):
                update_svn_dir(f, e)
            elif os.path.isdir(f) and os.listdir(f):
                # f is not empty
                if force_init:
                    clean_dir(f)
                    init_svn_dir(f, e)
                else:
                    repo.ui.warn('Can\'t checkout into non-empty dirs. ',
                        'Ignoring {0}.'.format(e.full_target), '\n\n')
            else:
                init_svn_dir(f, e)
        else:
            init_svn_dir(f, e)

def check_externals(repo, exts):
    svnclient = svn.SvnClient(repo)
    
    def check_svn_dir(path, ext):
        repo.ui.status("Checking status of '{0}':".format(ext.full_target), "\n")
        svnclient.call(['status'], cwd=path)
        repo.ui.status("\n")
    
    for e in exts:
        f = os.path.join(repo.repo_root, e.full_target)
        if os.path.isdir(f):
            if os.path.isdir(os.path.join(f, '.svn')):
                check_svn_dir(f, e)
            else:
                repo.ui.warn("Ignoring '{0}' - not a SVN dir.".format(e.full_target), "\n\n")
        else:
            repo.ui.warn("Ignoring '{0}' - not checked out.".format(e.full_target), "\n\n")
            
def write_ignores(repo, ignores):
    start_pat = '## BEGIN gitsvnext ignores'
    end_pat = '## END gitsvnext ignores'
    
    ignores_path = os.path.join(repo.git_dir, 'info/exclude')
    ignores_path_new = ignores_path + '.new'
    
    def write_ignores_lines(f):
        f.write(start_pat + '\n')
        f.writelines(ig + '\n' for ig in ignores)
        f.write(end_pat + '\n')
    
    with _create_file(ignores_path_new, 'w') as ignores_out: 
        start_hit = end_hit = False
        ignores_written = False
        with open(ignores_path) as ingores_in:
            for line in ingores_in:
                start_hit = start_hit or line.startswith(start_pat)
                    
                if not start_hit or end_hit:
                    ignores_out.write(line)
                elif not ignores_written:
                    repo.ui.note('Replacing existing ignore section...', '\n')
                    write_ignores_lines(ignores_out)
                    ignores_written = True
                    
                end_hit = end_hit or line.startswith(end_pat)
                
        if not ignores_written:
            repo.ui.note('Creating new ignore section...', '\n')
            ignores_out.write('\n')
            write_ignores_lines(ignores_out)
        
    _replace_file(ignores_path, ignores_path_new)
    
def convert_diff(diff, output=sys.stdout, svn_parent=None, svn_child=None):
    '''Converts git diff to svn diff
    '''
    newfile = False
    difflines = diff.splitlines(True)
    
    ADDED = '+++ {path}\t({rev})\n'
    REMOVED = '--- {path}\t({rev})\n'
    path = None
    
    svn_parent = ('revision ' + svn_parent if svn_parent else 'base revision')
    svn_child = ('revision ' + svn_child if svn_child else 'working copy')
    
    for line in difflines:
        # match the first line
        m = re.match(r'^diff --git a/(?P<path>.*) b/(?P=path)\s*$', line)
        if m:
            path = m.groupdict()['path']
            output.write('Index: {path}\n'.format(path=path))
            continue

        # match the second line
        if line.startswith('index '):
            output.write(63*'=')
            output.write('\n')
            continue
        
        if line.startswith('new file mode') or line.startswith('deleted file mode '):
          continue

        # match the third line
        if line.startswith('--- /dev/null'):
            newfile = True
            continue
            
        if line.startswith('+++ /dev/null'):
            output.write(ADDED.format(path=path, rev=svn_child))
            continue 

        m = re.match(r'^--- a/(?P<path>.*)\s*$', line)
        if m:
            newfile = False
            path = m.groupdict()['path'] # not needed - path from frist line should be enough
            output.write(REMOVED.format(path=path, rev=svn_parent))
            continue

        # match the fourth line
        m = re.match(r'^\+\+\+ b/(?P<path>.*)\s*$', line)
        if m:
            path = m.groupdict()['path'] # not needed - path from frist line should be enough
            if newfile:
                output.write(REMOVED.format(path=path, rev='revision 0'))
                output.write(ADDED.format(path=path, rev='revision 0'))
                continue
            else:
                output.write(ADDED.format(path=path, rev=svn_child))
                continue

        # pass everything else through
        output.write(line)

##-----------------------------------------------------------------------------
# commands definition

_reload_opt = ('L', 'reload', False, 'Reload externals definitions from SVN')
_svn_rev_opt = ('r', 'svn-rev', None, "SVN revision to load externals definitions from")

_repo_opts = [
    ('R', 'repo', '', 'repository path'),
]
  
register_plain = mcommand.register_with_ui_opts

def repo_proxy(func, ui, *args, **opts):
    ui, opts = mcommand.process_ui_opts(ui, opts)
    
    repo = git.load_repo(opts.pop('repo', None))
    repo.ui = ui
    repo.svn_rev = opts.get('svn_rev')
    
    return mcommand.checked_command_call(func, repo, *args, **opts)
    
def register(name, switches, usage):
    return mcommand.register_with_ui_opts(name, switches + _repo_opts, usage, proxy=repo_proxy)

@register('list-externals|lsext', [
        _reload_opt,
        ('t', 'targets', False, 'show (only) externals targets'),
        _svn_rev_opt,
    ], '%prog list-externals [OPTION]...')
def list_(repo, **opts):
    '''List defined SVN externals
    '''
    exts = load_externals(repo, reload=opts.get('reload'))
    for e in exts:
        if opts.get('targets'):
            s = e.full_target
        else:
            s = str(e)
        repo.ui.write(s, '\n')
        
@register('update-externals|upext', [
        _reload_opt,
        _svn_rev_opt,
        ('f', 'force', False, 'Perform an update even if directory for given external '
            + 'is not empty and is not a SVN working copy'),
    ], '%prog update-externals [OPTION]...')
def update(repo, **opts):
    '''Updates svn externals according to definitions
    
    Definitions of known externals are refreshed from svn.
    '''
    exts = load_externals(repo, reload=opts.get('reload'))
    update_externals(repo, exts, force_init=opts.get('force'))
    
@register('status-of-externals|stext', [
        _reload_opt,
    ], '%prog status-of-externals [OPTION]...')
def check(repo, **opts):
    '''Checks status of externals
    '''
    exts = load_externals(repo, reload=opts.get('reload'))
    check_externals(repo, exts)
    
@register('reload-externals', [
    ], '%prog reload-externals [OPTION]...')
def reload(repo, **opts):
    '''Reload externals definitions
    '''
    load_externals(repo, reload=True)
    
@register('write-ignores|wrig', [
        _reload_opt,
    ], '%prog write-ignores [OPTION]...')
def update_ignores(repo, **opts):
    '''Updates git ignore file to hide svn externals
    
    Curently is is acheieved by adding all the externals in .git/info/exclude.
    '''
    reload = opts.get('reload')
    exts = load_externals(repo, reload=reload)
    if not reload:
        # reload already caused ignores update
        ignores = (ext.full_target for ext in exts)
        write_ignores(repo, ignores)

@register('diff', [
        ('c', 'change', '', 'change made by revision')
    ], '%prog diff [OPTION]... ([-c REV] | PARENT [CHILD])')
def show_diff(repo, *revs, **opts):
    '''Display diff in svn format
    
    If not revisions are specified a difference to tracked branch is shown.
    If not branch is tracked diff to first SVN remote is used
    '''
    if opts.get('change'):
        if revs:
            raise mcommand.InvalidParams("--change cant't be used when revs are specified")
        revs = [opts['change'] + '~1', opts['change']]
    
    if len(revs) > 2:
        raise mcommand.InvalidParams('Too much arguments')
    elif not revs:
        # or we use the last svn merge point
        branch = repo.get_active_branch()
        svn_remote = repo.get_upstream_for(branch) if branch else None
        if svn_remote:
            repo.ui.note("Current branch is based on {0}.".format(svn_remote), "\n")
        else:
            svn_remote = 'remotes/{0}'.format(repo.get_svn_remotes()[0])
        
        svn_parent = repo.mcommand('merge-base', 'HEAD', svn_remote)
        repo.ui.note("Comparing working copy with it's parent in {0} ({1}).".format(svn_remote, svn_parent), "\n")
        revs = [svn_parent]

    # find the svn rev of that treeish
    svn_parent = repo.svn('find-rev', revs[0]).strip()
    if not svn_parent:
        repo.ui.status("Can't find SVN rev for the parent ({0})".format(revs[0]), "\n")
    if len(revs) > 1:
        svn_child = repo.svn('find-rev', revs[1]).strip()
        if not svn_child:
            repo.ui.status("Can't find SVN rev for the child ({0})".format(revs[1]), "\n")
    else:
        svn_child = None

    # generate a diff and then fix it up to look like a svn diff
    diff = repo.diff(*revs)
    
    convert_diff(diff, output=sys.stdout, svn_parent=svn_parent, svn_child=svn_child)

@register_plain('convert-diff|cdiff', [
        ('o', 'output-dir', '', 'Directory to place the converted diffs to'),
    ], '%prog convert-diff [OPTION]... DIFF_FILE...')
def convert_diff_files(ui, *files, **opts):
    '''Converts git diff files in svn format
    '''
    if not files:
        raise mcommand.InvalidParams("No files specified")
    output_dir = opts.get('output_dir')
    
    for f in files:
        if output_dir:
            output_path = os.path.join(output_dir, os.path.basename(f))
            output = open(output_path, 'w')
        else:
            output=sys.stdout
        diff = open(f).read()
        convert_diff(diff, output=output)
    
    ui.status("{0} file(s) processed...".format(len(files)), "\n")
    
def run(args):
    return mcommand.run(mcommand.BaseUi(), args[0], args[1:])
    
def main():
    exit(run(sys.argv))
